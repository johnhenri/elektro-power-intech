Attribute VB_Name = "Module2"
'******************************************************************************'
' Project           : Ingeteam / Elektro
' Author            : leandro.pedro@intech-automacao.com.br
' Date              : 2017/01/25
'
' Revision history
'------------------------------------------------------------------------------'
' Date          Author  Revision
' 2017/01/25    lbp     First commit.
' 2017/04/12    lbp     Added support for setpoints.
' 2018/07/18    lbp     Adjusted 61850 ParamItem for 'opening time' (TEMPO_AB).
' 2018/11/01    lbp     Auxgateway updated to xoComandoGenerico.
' 2019/10/09    lbp     Atualizacao macro padrao spectrum.
' 2020/05/29    jhm     Atualizacao Analogicas padrao Neo
' 2020/07/07    jhm     Atualizacao docstring, analogicas nao reconhecidas N4
' 2020/07/14    fsk     Atualizacao coluna INCLUIDO B.D. e remocao espacos tag
'------------------------------------------------------------------------------'
' Based on GeraBase() developed by luis.alves@intech-automacao.com.br
'
' Copyright (c) 2017 by In-Tech Automacao & Sistemas. All Rights Reserved.
'******************************************************************************'

Option Explicit

Public msg As String


Private Function GetAttribute(ByVal str As String) As String
    Dim regex As Object
    Set regex = CreateObject("VBScript.RegExp")
    
    With regex
        .Global = True
        .MultiLine = False
        .IgnoreCase = False
        .Pattern = "^(Op|TrDev.*[0-9]|Tr|OpCls|RecOK|RestST|RecCyc.*[0-9]|DefTrip)$"
    End With

    GetAttribute = IIf(regex.Test(str), "general", "stVal")
End Function

Private Function GetColumnNumberByName(ByVal objWs As Worksheet, columnName As String) As Integer
    Dim r As Range

    Set r = objWs.Range("A:Z").Find(What:=columnName, LookIn:=xlValues)

    If r Is Nothing Then
        ' If r isn't something (prevent errors).
        GetColumnNumberByName = 0
    Else
        ' Set to the first time a match is found.
        GetColumnNumberByName = r.Column
    End If
End Function

Private Sub ExportToCsv(shtName, csvPath As String, Optional overwrite As Variant)
    Const DELIMITER = ";"
    Dim fileName As String
    Dim lastColumn, columnLoop, fileNum As Integer
    Dim lastRow, rowLoop As Long
         
    On Error GoTo ErrorHandler:
         
    fileNum = FreeFile
    fileName = csvPath & "\" & shtName & ".csv"
    
    If (Dir(csvPath, vbDirectory) = vbNullString) Then
        MkDir (csvPath)
    End If
    
    ' Check if file already exists.
    If Dir(fileName) <> "" Then
        If Not IsNumeric(overwrite) Then
            ' If overwrite arg is null, ask to user to overwrite.
            If MsgBox(fileName & " will be deleted if you click OK", vbOKCancel) = vbCancel Then
                Exit Sub
            End If
        ElseIf Not overwrite Then
            ' If overwrite has been passed as negative.
            Exit Sub
        End If
    End If
     
    With Sheets(shtName).Cells
        lastColumn = .Find("*", [A1], , , xlByColumns, xlPrevious).Column
        lastRow = .Find("*", [A1], , , xlByRows, xlPrevious).row
    End With
     
    Open fileName For Output As #fileNum
     
    For rowLoop = 1 To lastRow
        For columnLoop = 1 To lastColumn
            Print #fileNum, Sheets(shtName).Cells(rowLoop, columnLoop).Value & DELIMITER;
        Next columnLoop
        Print #fileNum,
    Next rowLoop
     
    Close #fileNum
    
    Exit Sub
    
ErrorHandler:
    If (Err.Number = 70) Then
        MsgBox "The file: " & fileName & " is opened by another program.", vbExclamation, "Attention"
    End If
End Sub

Private Sub SweepSheets()
    Dim arr As Variant
    Dim d As Variant
    Dim paramDevice, DocString, paramItem, tag, TagType As String
    Dim included As Boolean
    Dim i As Integer
    Dim lastRow As Integer
    ' Dim msg As String
    Dim str() As String
    Dim objWs As Worksheet
    Dim key As Variant
    Dim auxType As String
    Dim colInclude As Variant
    
    Set d = CreateObject("Scripting.Dictionary")
    ' msg = vbNullString
    
    For Each objWs In ThisWorkbook.Sheets
        
        If (CBool(objWs.Visible)) Then ' Tags on hidden sheets won't be generated.
            ' Clear active filter.
            If objWs.FilterMode Then
                objWs.ShowAllData
            End If
            
            lastRow = objWs.UsedRange.SpecialCells(xlCellTypeLastCell).row
                        
            For i = 1 To lastRow
                tag = Replace(objWs.Cells(i, "F").Value, " ", "")
                colInclude = GetColumnNumberByName(objWs, "INCLUIDO B.D.")
                If (colInclude) Then
                    included = LCase(objWs.Cells(i, colInclude).Value) Like "s*"
                Else
                    included = False
                End If
                
                If (tag <> vbNullString And included) Then

                    ' ".#.#" increase string array size.
                    paramItem = objWs.Cells(i, GetColumnNumberByName(objWs, "LOGICAL NODE")).Value & ".#.#"
                    paramDevice = objWs.Cells(i, GetColumnNumberByName(objWs, "IED")).Value
                    DocString = objWs.Cells(i, "G").Value
                    TagType = StrConv(objWs.Name, vbProperCase)
                    If (TagType Like "Anal*gic*") Then
                         TagType = "Analogicos"
                         DocString = ""
                    End If
                    auxType = objWs.Cells(i, "A").Value
                    str() = Split(Replace(paramItem, "/", "."), ".") '
                    
                     If UBound(str) > 5 Then
                        ' Format "paramItem" on Elipse format.
                        Select Case TagType
                            Case "Analogicos"
                                If UBound(str) = 7 Then
                                    If (str(3) = "OpCnt") Then
                                        paramItem = str(2) & "$ST$OpCnt$stVal"
                                    Else
                                        paramItem = str(2) & "$" & str(5) & "$" & str(3) & "$" & str(4) & "$cVal$mag$f"
                                    End If

                                ElseIf UBound(str) = 6 Then
                                    paramItem = str(2) & "$" & str(4) & "$" & str(3) & "$mag$f"
                                End If
                            Case Else
                                If str(4) = "#" Then
                                    str(4) = "ST"
                                ElseIf str(5) = "#" Then
                                    str(5) = GetAttribute(str(3))
                                End If
                                paramItem = str(2) & "$" & str(4) & "$" & str(3) & "$" & str(5)
                        End Select
                        
                        paramDevice = str(0) & ":" & str(0) & str(1)
                                                
                        If (Not d.Exists(tag)) Then
                            arr = Array(tag, DocString, paramDevice, paramItem, TagType, auxType)
                            ' arr(0) = tag
                            ' arr(1) = docString
                            ' arr(2) = paramDevice
                            ' arr(3) = paramItem
                            ' arr(4) = tagType
                            d.Add tag, arr
                        Else
                            msg = tag & vbNewLine & msg
                        End If
                        
                    End If
                End If
            Next i
        End If
    Next
    
    Call Iec61850(d)    ' IEC61850.
    Call DNP3Slave(d)   ' DNPSlave.
    Call DNPMaster(d)   ' DNPMaster.
    Call AuxGateway(d)  ' AuxGateway.
End Sub

''''''''''''''''''''''''''''''''''' IEC61850 '''''''''''''''''''''''''''''''''''
Sub Iec61850(d As Variant)
    
    If (TypeName(d) <> "Dictionary") Then Exit Sub
    
    Dim key As Variant
    Dim objWs As Worksheet
    Dim row As Integer
    Dim auxType As String
    Dim auxFolder, tag, DocString, paramDevice, paramItem, TagType
    Dim dict As Object
  
    Set dict = CreateObject("Scripting.Dictionary")
    
    Set objWs = ThisWorkbook.Sheets.Add(after:=ThisWorkbook.Worksheets(ThisWorkbook.Worksheets.Count))
    objWs.Name = "IEC61850"
        
    row = 1
    
    objWs.Cells(row, "A").Value = "ObjectType"
    objWs.Cells(row, "B").Value = "AdviseType"
    objWs.Cells(row, "C").Value = "AllowRead"
    objWs.Cells(row, "D").Value = "AllowWrite"
    objWs.Cells(row, "E").Value = "DeadBand"
    objWs.Cells(row, "F").Value = "DocString"
    objWs.Cells(row, "G").Value = "Name"
    objWs.Cells(row, "H").Value = "ParamDevice"
    objWs.Cells(row, "I").Value = "ParamItem"
    
    ' Keep 2nd row blank.
    row = 3
    
    For Each key In d.Keys
        tag = d(key)(0)
        DocString = d(key)(1)
        paramDevice = d(key)(2)
        paramItem = d(key)(3)
        TagType = d(key)(4)
        auxType = d(key)(5)
        
        If (Not dict.Exists(Split(paramDevice, ":")(0))) Then
            dict.Add Split(paramDevice, ":")(0), 0
        End If
        
        If (auxFolder <> TagType) Then
            auxFolder = TagType
            objWs.Cells(row, "A").Value = "IOFolder"   ' ObjectType.
            objWs.Cells(row, "G").Value = auxFolder    ' Name.
            row = row + 1
        End If
        
        objWs.Cells(row, "A").Value = "IOTag"                          ' ObjectType.
        objWs.Cells(row, "B").Value = 0                                ' AdviseType.
        objWs.Cells(row, "C").Value = IIf(InStr(TagType, "Comandos") Or _
                InStr(TagType, "SP"), "'False", "'True")            ' AllowRead.
        objWs.Cells(row, "D").Value = IIf(InStr(TagType, "Comandos") Or _
                InStr(TagType, "SP"), "'True", "'False")            ' AllowWrite.
        objWs.Cells(row, "E").Value = "'False"                         ' DeadBand.
        objWs.Cells(row, "F").Value = DocString                        ' DocString.
        objWs.Cells(row, "G").Value = auxFolder & "." & tag            ' Name.
        objWs.Cells(row, "H").Value = paramDevice                           ' ParamDevice.
        objWs.Cells(row, "I").Value = paramItem                             ' ParamItem.
        
        row = row + 1
    Next key
    
    ' SisInfo.
    objWs.Cells(row, "A").Value = "IOFolder"   ' ObjectType.
    objWs.Cells(row, "G").Value = "SisInfo"    ' Name.

    For Each key In dict.Keys
        row = row + 1
        objWs.Cells(row, "A").Value = "IOTag"              ' ObjectType.
        objWs.Cells(row, "B").Value = "0"                  ' Advise Type.
        objWs.Cells(row, "C").Value = "'True"              ' AllowRead.
        objWs.Cells(row, "D").Value = "'False"             ' AllowWrite.
        objWs.Cells(row, "F").Value = _
                Split(key, "_")(0) & " - " & _
                Split(key, "_")(1) & _
                " - Falha de comunicacao"               ' DocString.
        objWs.Cells(row, "E").Value = "'False"             ' Deadband.
        objWs.Cells(row, "G").Value = "SisInfo." & key     ' Name.
        objWs.Cells(row, "H").Value = key                  ' ParamDevice.
        objWs.Cells(row, "I").Value = "ServerStatus"       ' ParamItem.
    Next key
    
    ' Export to csv file.
    Call ExportToCsv(objWs.Name, ThisWorkbook.Path, True)
        
    Application.DisplayAlerts = False
    objWs.Delete
    Application.DisplayAlerts = True
End Sub
'''''''''''''''''''''''''''''''''' /IEC61850/ ''''''''''''''''''''''''''''''''''

'''''''''''''''''''''''''''''''''' DNP3Slave '''''''''''''''''''''''''''''''''''
Sub DNP3Slave(d As Variant)
    If (TypeName(d) <> "Dictionary") Then Exit Sub
    
    Dim key As Variant
    Dim objWs As Worksheet
    Dim row, auxAdress As Integer
    Dim address, N1, N2, N3, N4 As Integer
    Dim auxType As String
    Dim auxFolder, tag, DocString, paramDevice, paramItem, TagType
    Dim dict As Object
    
    Set dict = CreateObject("Scripting.Dictionary")
    
    Set objWs = ThisWorkbook.Sheets.Add(after:=ThisWorkbook.Worksheets(ThisWorkbook.Worksheets.Count))
    objWs.Name = "DNP3Slave"
        
    row = 1
    auxAdress = 0
    
    ' DNP3Slave fields.
    objWs.Cells(row, "A").Value = "ObjectType"
    objWs.Cells(row, "B").Value = "AdviseType"
    objWs.Cells(row, "C").Value = "AllowRead"
    objWs.Cells(row, "D").Value = "AllowWrite"
    objWs.Cells(row, "E").Value = "EnableDeadband"
    objWs.Cells(row, "F").Value = "EnableDriverEvent"
    objWs.Cells(row, "G").Value = "DocString"
    objWs.Cells(row, "H").Value = "Name"
    objWs.Cells(row, "I").Value = "N1"
    objWs.Cells(row, "J").Value = "N2"
    objWs.Cells(row, "K").Value = "N3"
    objWs.Cells(row, "L").Value = "N4"
    objWs.Cells(row, "M").Value = "Source"
    
    ' Keep 2nd row blank.
    row = 3
    
    For Each key In d.Keys
        
        tag = d(key)(0)
        DocString = d(key)(1)
        paramDevice = d(key)(2)
        paramItem = d(key)(3)
        TagType = d(key)(4)
        auxType = d(key)(5)
        
        If (Not dict.Exists(Split(paramDevice, ":")(0))) Then
            dict.Add Split(paramDevice, ":")(0), 0
        End If
        
        If (auxFolder <> TagType) Then
            auxFolder = TagType
            objWs.Cells(row, "A").Value = "IOFolder"   ' ObjectType.
            objWs.Cells(row, "H").Value = auxFolder    ' Name.
            row = row + 1
            address = 0
        End If
        ' N params for DNP3Slave.
        Select Case True
            Case UCase(auxType) Like "DUPLO"
                N1 = 21
                N2 = 1
                N3 = 402
                auxAdress = address
            Case UCase(auxType) Like "SIMPLES"
                N1 = 21
                N2 = 1
                N3 = 202
                auxAdress = address
            Case UCase(auxType) Like "ANAL?GICO*"
                N1 = 12
                N2 = 1
                N3 = 3205
            Case UCase(auxType) Like "COMANDO*"
                N1 = 12
                N2 = 5
                N3 = 1201
            Case UCase(auxType) Like "SETPOINT*"
                N1 = 12
                N2 = 5
                N3 = 4103
            Case Else
        End Select
              
        objWs.Cells(row, "A").Value = "IOTag"                          ' ObjectType.
        objWs.Cells(row, "B").Value = 0                                ' AdviseType.
        objWs.Cells(row, "C").Value = IIf(InStr(TagType, "Comandos") Or _
                InStr(TagType, "SP"), "'True", "'False")            ' AllowRead.
        objWs.Cells(row, "D").Value = IIf(InStr(TagType, "Comandos") Or _
                InStr(TagType, "SP"), "'False", "'True")            ' AllowWrite.
        objWs.Cells(row, "E").Value = "'False"                         ' EnableDeadband.
        objWs.Cells(row, "F").Value = IIf(InStr(TagType, "Comandos") Or _
                InStr(TagType, "SP"), "'True", "'False")            ' EnableDriverEvent.
        objWs.Cells(row, "G").Value = DocString                        ' DocString.
        objWs.Cells(row, "H").Value = auxFolder & "." & tag            ' Name.
        objWs.Cells(row, "I").Value = N1                               ' N1.
        objWs.Cells(row, "J").Value = N2                               ' N2.
        objWs.Cells(row, "K").Value = N3                               ' N3.
        objWs.Cells(row, "L").Value = address                          ' N4.
        
        If (TagType <> "Comandos" And TagType <> "SP") Then
            row = row + 1
            objWs.Cells(row, "A").Value = "AgSimple"                               ' ObjectType.
            objWs.Cells(row, "H").Value = auxFolder & "." & tag & ".Links.Value"   ' Name.
            objWs.Cells(row, "M").Value = "Aquisicao.[61850].IEC61850." & _
                    auxFolder & "." & tag & ".Value"                            ' Associations (Source).
        End If
        
        address = address + 1
        row = row + 1
    Next
    
    ' SisInfo.
    objWs.Cells(row, "A").Value = "IOFolder"   ' ObjectType.
    objWs.Cells(row, "H").Value = "SisInfo"    ' Name.
    
    For Each key In dict.Keys
        row = row + 1
        auxAdress = auxAdress + 1
        
        objWs.Cells(row, "A").Value = "IOTag"          ' ObjectType.
        objWs.Cells(row, "B").Value = 0                ' AdviseType.
        objWs.Cells(row, "C").Value = "'False"         ' AllowRead.
        objWs.Cells(row, "D").Value = "'True"          ' AllowWrite.
        objWs.Cells(row, "E").Value = "'False"         ' EnableDeadband.
        objWs.Cells(row, "F").Value = "'False"         ' EnableDriverEvent.
        objWs.Cells(row, "G").Value = _
                Split(key, "_")(0) & " - " & _
                Split(key, "_")(1) & _
                " - Falha de comunicacao"           ' DocString.
        objWs.Cells(row, "H").Value = "SisInfo." & key ' Name.
        objWs.Cells(row, "I").Value = 21               ' N1.
        objWs.Cells(row, "J").Value = 1                ' N2.
        objWs.Cells(row, "K").Value = 202              ' N3.
        objWs.Cells(row, "L").Value = auxAdress        ' N4.
        
        row = row + 1
    
        objWs.Cells(row, "A").Value = "AgSimple"       ' ObjectType.
        objWs.Cells(row, "H").Value = _
            "SisInfo." & key & ".Links.Value"       ' Name.
        ' Associations (Source).
        objWs.Cells(row, "M").Value = _
                "(-1) * (CInt(Aquisicao.[61850].IEC61850." & _
                "SisInfo." & key & ".Value <> 5))"
    Next key
    ' /SisInfo.
    
    ' Export to csv file.
    Call ExportToCsv(objWs.Name, ThisWorkbook.Path, True)
    
    Application.DisplayAlerts = False
    objWs.Delete
    Application.DisplayAlerts = True
End Sub
''''''''''''''''''''''''''''''''' /DNP3Slave/ ''''''''''''''''''''''''''''''''''

'''''''''''''''''''''''''''''''''' DNP3Master ''''''''''''''''''''''''''''''''''
Sub DNPMaster(d As Variant)
    If (TypeName(d) <> "Dictionary") Then Exit Sub
    
    Dim key                         As Variant
    Dim objWs                          As Worksheet
    Dim row, auxAdress              As Integer
    Dim address, N1, N2, N3, N4     As Integer
    Dim auxType As String
    Dim auxFolder, tag, DocString, paramDevice, paramItem, TagType
    Dim dict As Object
    
    Set dict = CreateObject("Scripting.Dictionary")
    
    Set objWs = ThisWorkbook.Sheets.Add(after:=ThisWorkbook.Worksheets(ThisWorkbook.Worksheets.Count))
    objWs.Name = "DNP3Master"
        
    row = 1
    auxAdress = 0
    
    ' DNP3Master fields.
    objWs.Cells(row, "A").Value = "ObjectType"
    objWs.Cells(row, "B").Value = "AdviseType"
    objWs.Cells(row, "C").Value = "AllowRead"
    objWs.Cells(row, "D").Value = "AllowWrite"
    objWs.Cells(row, "E").Value = "EnableDeadband"
    objWs.Cells(row, "F").Value = "EnableDriverEvent"
    objWs.Cells(row, "G").Value = "DocString"
    objWs.Cells(row, "H").Value = "Name"
    objWs.Cells(row, "I").Value = "N1"
    objWs.Cells(row, "J").Value = "N2"
    objWs.Cells(row, "K").Value = "N3"
    objWs.Cells(row, "L").Value = "N4"
    objWs.Cells(row, "M").Value = "Source"
    
    ' Keep 2nd row blank.
    row = 3
    
    For Each key In d.Keys
        
        tag = d(key)(0)
        DocString = d(key)(1)
        paramDevice = d(key)(2)
        paramItem = d(key)(3)
        TagType = d(key)(4)
        auxType = d(key)(5)
        
        If (Not dict.Exists(Split(paramDevice, ":")(0))) Then
            dict.Add Split(paramDevice, ":")(0), 0
        End If
        
        If (auxFolder <> TagType) Then
            auxFolder = TagType
            objWs.Cells(row, "A").Value = "IOFolder"   ' ObjectType.
            objWs.Cells(row, "H").Value = auxFolder    ' Name.
            row = row + 1
            address = 0
        End If
                
        ' N params for DNP3Master.
        N1 = 1
        Select Case True
            Case UCase(auxType) Like "DUPLO"
                ' n1 = 1
                N2 = 1
                N3 = 402
                auxAdress = address
            Case UCase(auxType) Like "SIMPLES"
                ' n1 = 1
                N2 = 1
                N3 = 202
                auxAdress = address
            Case UCase(auxType) Like "ANAL?GICO*"
                ' n1 = 1
                N2 = 1
                N3 = 3205
            Case UCase(auxType) Like "COMANDO*"
                ' n1 = 1
                N2 = 5
                N3 = 1201
            Case UCase(auxType) Like "SETPOINT*"
                ' n1 = 1
                N2 = 5
                N3 = 4103
            Case Else
        End Select
              
        objWs.Cells(row, "A").Value = "IOTag"                          ' ObjectType.
        objWs.Cells(row, "B").Value = 1                                ' AdviseType.
        objWs.Cells(row, "C").Value = IIf(InStr(TagType, "Comandos") Or _
                InStr(TagType, "SP"), "'False", "'True")            ' AllowRead.
        objWs.Cells(row, "D").Value = IIf(InStr(TagType, "Comandos") Or _
                InStr(TagType, "SP"), "'True", "'False")            ' AllowWrite.
        objWs.Cells(row, "E").Value = "'False"                         ' EnableDeadband.
        objWs.Cells(row, "F").Value = "'False"                         ' EnableDriverEvent.
        objWs.Cells(row, "G").Value = DocString                        ' DocString.
        objWs.Cells(row, "H").Value = auxFolder & "." & tag            ' Name.
        objWs.Cells(row, "I").Value = N1                               ' N1.
        objWs.Cells(row, "J").Value = N2                               ' N2.
        objWs.Cells(row, "K").Value = N3                               ' N3.
        objWs.Cells(row, "L").Value = address                          ' N4.
            
        address = address + 1
        row = row + 1
    Next
    
    'SisInfo.
    objWs.Cells(row, "A").Value = "IOFolder"
    objWs.Cells(row, "H").Value = "SisInfo"
    
    For Each key In dict.Keys
        row = row + 1
        auxAdress = auxAdress + 1
        
        objWs.Cells(row, "A").Value = "IOTag"          ' ObjectType.
        objWs.Cells(row, "B").Value = "0"              ' Advise Type.
        objWs.Cells(row, "C").Value = "'True"          ' AllowRead.
        objWs.Cells(row, "D").Value = "'False"         ' AllowWrite.
        objWs.Cells(row, "E").Value = "'False"         ' EnableDeadband.
        objWs.Cells(row, "F").Value = "'False"         ' EnableDriverEvent.
        objWs.Cells(row, "G").Value = _
                Split(key, "_")(0) & " - " & _
                Split(key, "_")(1) & _
                    " - Falha de comunicacao"       ' DocString.
        objWs.Cells(row, "H").Value = "SisInfo." & key ' Name.
        objWs.Cells(row, "I").Value = 1                ' N1.
        objWs.Cells(row, "J").Value = 1                ' N2.
        objWs.Cells(row, "K").Value = 202              ' N3.
        objWs.Cells(row, "L").Value = auxAdress        ' N4.
    Next key
    ' /SisInfo.
    
    ' Export to csv file.
    Call ExportToCsv(objWs.Name, ThisWorkbook.Path, True)
    
    Application.DisplayAlerts = False
    objWs.Delete
    Application.DisplayAlerts = True
End Sub
''''''''''''''''''''''''''''''''' /DNP3Master/ '''''''''''''''''''''''''''''''''

'''''''''''''''''''''''''''''''''' AuxGateway ''''''''''''''''''''''''''''''''''
Private Sub AuxGateway(d As Variant)
    If (TypeName(d) <> "Dictionary") Then Exit Sub
    
    Dim key As Variant
    Dim mname, pathContainer, pathName, pathVolume, ObjectType, tagDnp, tagIec As String
    Dim row, i, j, index, lastRow As Integer
    Dim objWs As Worksheet
    Dim DocString, paramDevice, paramItem, tag, TagType As String
    Dim auxType As String
    
    Set objWs = ThisWorkbook.Sheets.Add(after:=ThisWorkbook.Worksheets(ThisWorkbook.Worksheets.Count))
    objWs.Name = "AuxGateway"
        
    row = 1
    
    ' AuxGateway fields.
    objWs.Cells(row, "A").Value = "ObjectType"
    objWs.Cells(row, "B").Value = "DocString"
    objWs.Cells(row, "C").Value = "Name"
    objWs.Cells(row, "D").Value = "CloseOutputValue"
    objWs.Cells(row, "E").Value = "InputTag"
    objWs.Cells(row, "F").Value = "OutputTag"
    objWs.Cells(row, "G").Value = "TripOutputValue"
    
    ' DNP folder.
    row = row + 1
    objWs.Cells(row, "A").Value = "DataFolder" ' ObjectType.
    objWs.Cells(row, "C").Value = "DNP"        ' Name.
    
    ' DNP3Slave folder.
    row = row + 1
    objWs.Cells(row, "A").Value = "DataFolder"     ' ObjectType.
    objWs.Cells(row, "C").Value = "DNP.DNP3Slave"  ' Name.
    
    For Each key In d.Keys
        tag = d(key)(0)
        DocString = d(key)(1)
        paramDevice = d(key)(2)
        paramItem = d(key)(3)
        TagType = d(key)(4)
        auxType = d(key)(5)
        
        If (TagType = "Comandos") Then
            row = row + 1

            objWs.Cells(row, "A").Value = "xoComandoGenerico"                          ' ObjectType.
            objWs.Cells(row, "B").Value = DocString                                    ' DocString.
            objWs.Cells(row, "C").Value = "DNP.DNP3Slave." & tag                       ' Name.
            objWs.Cells(row, "D").Value = 1                                            ' CloseOutputValue.
            objWs.Cells(row, "E").Value = "Distribuicao.DNP3Slave.Comandos." & tag     ' InputTag.
            objWs.Cells(row, "F").Value = "Aquisicao.[61850].IEC61850.Comandos." & tag ' OutputTag.
            objWs.Cells(row, "G").Value = 0                                            ' TripOutputValue.
        End If
    Next key
    
    ' Export to csv file.
    Call ExportToCsv(objWs.Name, ThisWorkbook.Path, True)
    
    Application.DisplayAlerts = False
    objWs.Delete
    Application.DisplayAlerts = True
End Sub
''''''''''''''''''''''''''''''''' /AuxGateway/ '''''''''''''''''''''''''''''''''

Private Sub HideWorksheets()
    Dim regex As Object
    Dim objWs As Worksheet
    
    Set regex = CreateObject("VBScript.RegExp")
    regex.Global = True
    regex.IgnoreCase = True
    regex.Pattern = "^(Digitais|Anal[�o]gic[ao](s\b|\b)|Comandos|Setpoints)$"
        
    For Each objWs In ThisWorkbook.Worksheets
       objWs.Visible = regex.Test(objWs.Name)
    Next
End Sub
Sub CopyToClipboard(text As String)
    Dim obj As Object
    
    On Error Resume Next
    
    Set obj = CreateObject("new:{1C3B4210-F441-11CE-B9EA-00AA006B1A69}")
    obj.SetText text
    obj.PutInClipboard
    Set obj = Nothing
    
    On Error GoTo 0
End Sub
Sub GenerateDatabase()
    Dim t
    Dim i As Double
    
    Application.ScreenUpdating = False
    t = Timer()
    msg = vbNullString
    
    Call HideWorksheets
    
    Call SweepSheets
    
    CopyToClipboard (ThisWorkbook.Path)
    Sheets(1).Select
    
    MsgBox "Database generated in " & Round(Timer - t, 2) & " s.", _
            vbInformation, ""
            
    If (msg <> vbNullString) Then
        MsgBox "Atencao, os tags a seguir se encontram repetidos: " & _
                vbNewLine & msg ', _
                'vbExclamation, _
                '"Atencao"
    End If
End Sub

'************************************ Foo *************************************'
' Description : For test purposes. Keep it in the end of this file.            '
'******************************************************************************'
Private Sub Foo()
End Sub
'*********************************** /Foo/ ************************************'
