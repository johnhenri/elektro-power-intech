VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "Tag"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'******************************************************************************'
' Project   : Ingeteam / Neoenergia
' File      : Tag.cls
' Author    : leandro.pedro@intech-automacao.com.br
' Date      : 2020/10/19
'
' Revision history
'------------------------------------------------------------------------------'
' Date      Author  Revision
' 20/10/19  lbp     First commit.
' 20/10/21  lbp     Upd AdviseType aquisicao e slave p/ AlwaysInAdvise
'                   Add FormatDocString
' 20/11/06  lbp     Fix N1/N2 for slave + Add EnableDeadband.
'------------------------------------------------------------------------------''
' Copyright (c) 2020 by In-Tech Automacao & Sistemas. All Rights Reserved.
'******************************************************************************'
Enum AdviseTypes
    AlwaysInAdvise = 0
    AdviseWhenLinked = 1
End Enum

Private m_ObjectType As String
Private m_Ref As String
Private m_IED As String
Private m_Name


' Identificacao
Public DocString As String
'Public Name As String

Public N4 As Integer


Private Sub Class_Initialize()

End Sub


Public Sub Init(ByVal ObjectType As String, _
                ByVal TagName As String, _
                ByVal sDocString As String, _
                ByVal Ref As String)

    m_ObjectType = ObjectType
    m_Name = TagName
    DocString = FormatDocString(sDocString)
    m_Ref = Ref
    m_IED = IED
End Sub

Public Property Get ObjectFunction()
    Select Case True
        Case m_ObjectType Like "SIMPLE?": ObjectFunction = "DS"
        Case m_ObjectType Like "DUPLO": ObjectFunction = "DD"
        Case m_ObjectType Like "ANAL?GIC*": ObjectFunction = "AN"
        Case m_ObjectType Like "COMANDO*": ObjectFunction = "CD"
    End Select
End Property

Public Property Get Name()
    Select Case ObjectFunction
        Case "DS", "DD":    Name = "Digitais.[" & m_Name & "]"
        Case "AN":          Name = "Analogicos.[" & m_Name & "]"
        Case "CD":          Name = "Comandos.[" & m_Name & "]"
    End Select
End Property

Public Property Get ObjectType()
    ObjectType = "IOTag"
End Property
Public Property Get IEC61850() As VBA.Collection
    Set IEC61850 = New VBA.Collection
    Select Case ObjectFunction
        Case "DS"
            IEC61850.Add Item:=True, key:="AllowRead"
            IEC61850.Add Item:=False, key:="AllowWrite"
            IEC61850.Add Item:=AlwaysInAdvise, key:="AdviseType"
            IEC61850.Add Item:=False, key:="EnableDriverEvent"
            ' IEC61850.Add Item:=1, key:="EnableDeadBand"

        Case "DD"
            IEC61850.Add Item:=True, key:="AllowRead"
            IEC61850.Add Item:=False, key:="AllowWrite"
            IEC61850.Add Item:=AlwaysInAdvise, key:="AdviseType"
            IEC61850.Add Item:=False, key:="EnableDriverEvent"
            ' IEC61850.Add Item:=1, key:="EnableDeadBand"

        Case "AN"
            IEC61850.Add Item:=True, key:="AllowRead"
            IEC61850.Add Item:=False, key:="AllowWrite"
            IEC61850.Add Item:=AlwaysInAdvise, key:="AdviseType"
            IEC61850.Add Item:=False, key:="EnableDriverEvent"
            ' IEC61850.Add Item:=1, key:="EnableDeadBand"

        Case "CD"
            IEC61850.Add Item:=False, key:="AllowRead"
            IEC61850.Add Item:=True, key:="AllowWrite"
            IEC61850.Add Item:=AlwaysInAdvise, key:="AdviseType"
            IEC61850.Add Item:=False, key:="EnableDriverEvent"
            ' IEC61850.Add Item:=1, key:="EnableDeadBand"
    End Select

    IEC61850.Add Item:=m_ParamItem, key:="ParamItem"
    IEC61850.Add Item:=m_ParamDevice, key:="ParamDevice"
End Property

Public Property Get Master() As VBA.Collection
    Set Master = New VBA.Collection
    Select Case ObjectFunction
        Case "DS"
            Master.Add Item:=1, key:="N1"
            Master.Add Item:=1, key:="N2"
            Master.Add Item:=202, key:="N3"
            Master.Add Item:=True, key:="AllowRead"
            Master.Add Item:=False, key:="AllowWrite"
            Master.Add Item:=AdviseWhenLinked, key:="AdviseType"
            Master.Add Item:=False, key:="EnableDriverEvent"
            Master.Add Item:=False, key:="EnableDeadBand"

        Case "DD"
            Master.Add Item:=1, key:="N1"
            Master.Add Item:=1, key:="N2"
            Master.Add Item:=402, key:="N3"
            Master.Add Item:=True, key:="AllowRead"
            Master.Add Item:=False, key:="AllowWrite"
            Master.Add Item:=AdviseWhenLinked, key:="AdviseType"
            Master.Add Item:=False, key:="EnableDriverEvent"
            Master.Add Item:=False, key:="EnableDeadBand"

        Case "AN"
            Master.Add Item:=1, key:="N1"
            Master.Add Item:=1, key:="N2"
            Master.Add Item:=3205, key:="N3"
            Master.Add Item:=True, key:="AllowRead"
            Master.Add Item:=False, key:="AllowWrite"
            Master.Add Item:=AdviseWhenLinked, key:="AdviseType"
            Master.Add Item:=False, key:="EnableDriverEvent"
            Master.Add Item:=False, key:="EnableDeadBand"

        Case "CD"
            Master.Add Item:=1, key:="N1"
            Master.Add Item:=5, key:="N2"
            Master.Add Item:=1201, key:="N3"
            Master.Add Item:=False, key:="AllowRead"
            Master.Add Item:=True, key:="AllowWrite"
            Master.Add Item:=AdviseWhenLinked, key:="AdviseType"
            Master.Add Item:=False, key:="EnableDriverEvent"
            Master.Add Item:=False, key:="EnableDeadBand"
    End Select
End Property

Public Property Get Slave() As VBA.Collection
    Set Slave = New VBA.Collection
    Select Case ObjectFunction
        Case "DS"
            Slave.Add Item:=21, key:="N1"
            Slave.Add Item:=1, key:="N2"
            Slave.Add Item:=202, key:="N3"
            Slave.Add Item:=False, key:="AllowRead"
            Slave.Add Item:=True, key:="AllowWrite"
            Slave.Add Item:=AlwaysInAdvise, key:="AdviseType"
            Slave.Add Item:=False, key:="EnableDriverEvent"
            Slave.Add Item:=False, key:="EnableDeadBand"

        Case "DD"
            Slave.Add Item:=21, key:="N1"
            Slave.Add Item:=1, key:="N2"
            Slave.Add Item:=402, key:="N3"
            Slave.Add Item:=False, key:="AllowRead"
            Slave.Add Item:=True, key:="AllowWrite"
            Slave.Add Item:=AlwaysInAdvise, key:="AdviseType"
            Slave.Add Item:=False, key:="EnableDriverEvent"
            Slave.Add Item:=False, key:="EnableDeadBand"

        Case "AN"
            Slave.Add Item:=12, key:="N1"
            Slave.Add Item:=1, key:="N2"
            Slave.Add Item:=3205, key:="N3"
            Slave.Add Item:=False, key:="AllowRead"
            Slave.Add Item:=True, key:="AllowWrite"
            Slave.Add Item:=AlwaysInAdvise, key:="AdviseType"
            Slave.Add Item:=False, key:="EnableDriverEvent"
            Slave.Add Item:=False, key:="EnableDeadBand"

        Case "CD"
            Slave.Add Item:=12, key:="N1"
            Slave.Add Item:=5, key:="N2"
            Slave.Add Item:=1201, key:="N3"
            Slave.Add Item:=True, key:="AllowRead"
            Slave.Add Item:=False, key:="AllowWrite"
            Slave.Add Item:=AlwaysInAdvise, key:="AdviseType"
            Slave.Add Item:=True, key:="EnableDriverEvent"
            Slave.Add Item:=False, key:="EnableDeadBand"
    End Select
End Property

Private Property Get m_ParamItem()
    Dim arr As Variant
    Dim s As String
    s = m_Ref & ".#.#"
    arr = Split(Replace(s, "/", "."), ".")
    If UBound(arr) > 5 Then
        ' Format Elipse format.
        If ObjectFunction = "AN" Then
            If UBound(arr) = 7 Then
                If (arr(3) = "OpCnt") Then
                    s = arr(2) & "$ST$OpCnt$stVal"
                Else
                    s = arr(2) & "$" & arr(5) & "$" & arr(3) & "$" & arr(4) & "$cVal$mag$f"
                End If
                
            ElseIf UBound(arr) = 6 Then
                s = arr(2) & "$" & arr(4) & "$" & arr(3) & "$mag$f"
            End If
        Else
            If arr(4) = "#" Then
                arr(4) = "ST"
            ElseIf arr(5) = "#" Then
                arr(5) = GetAttribute(arr(3))
            End If
            s = arr(2) & "$" & arr(4) & "$" & arr(3) & "$" & arr(5)
        End If
        
        m_ParamItem = s
        
    End If
End Property
Private Property Get m_ParamDevice()
    Dim arr As Variant
    arr = Split(Replace(m_Ref, "/", "."), ".")
    m_ParamDevice = arr(0) & ":" & arr(0) & arr(1)
End Property

Private Function GetAttribute(ByVal s As String) As String
    With CreateObject("VBScript.RegExp")
        .Global = True
        .MultiLine = False
        .IgnoreCase = False
        .Pattern = "^(Op|TrDev.*[0-9]|Tr|OpCls|RecOK|RestST|RecCyc.*[0-9]|DefTrip)$"
         GetAttribute = IIf(.Test(s), "general", "stVal")
    End With
End Function
' Remove acento da DocString
Private Function FormatDocString(ByRef Text As String)
    Const sFind = "����������������������������"
    Const sReplace = "EEEEUUIIAAOeeeeuuiiaaoCcAaOo"
    For i = 1 To Len(Text)
        pos = InStr(sFind, Mid(Text, i, 1))
        If pos > 0 Then
            Mid(Text, i, 1) = Mid(sReplace, pos, 1)
        End If
    Next
    FormatDocString = Text
End Function

