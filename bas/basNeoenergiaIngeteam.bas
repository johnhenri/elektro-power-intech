Attribute VB_Name = "basNeoenergiaIngeteam"
'******************************************************************************'
' Project   : Ingeteam / Neoenergia
' File      : basNeoenergiaIngeteam.bas
' Author    : leandro.pedro@intech-automacao.com.br
' Date      : 2020/10/19
'
' Revision history
'------------------------------------------------------------------------------'
' Date      Author  Revision
' 20/10/19  lbp     First commit.
' 20/10/20  lbp     Add AuxGateway + SisInfo
' 20/10/21  lbp     Rem filtro da planilha e tags tachado.
' 20/11/06  lbo     Fix master SinInfo + Add EnableDeadband.
'------------------------------------------------------------------------------''
' Copyright (c) 2020 by In-Tech Automacao & Sistemas. All Rights Reserved.
'******************************************************************************'

Option Explicit

Public g_Dict As Object

Enum IODrivers
    IEC61850 = 0
    DNP3Master = 1
    DNP3Slave = 2
    AuxGateway = 3
End Enum

Public Sub Xls2Elipse()
    Dim objWs As Worksheet
    Dim lastRow As Long
    Dim i As Long
    Dim isIncludedOnDatabase As Boolean
    Dim iColIncludedOnDatabase As Integer
    Dim iColType As Integer
    Dim iColIED As Integer
    Dim iColTag As Integer
    Dim iColDocstring As Integer
    Dim iColRef As Integer
    Dim address As Integer
    
    Dim sType As String
    Dim sIED As String
    Dim sTag As String
    Dim sDocString As String
    Dim sRef As String
    
    Dim objTag As Tag
    
    Dim listIEC61850 As Object:     Set listIEC61850 = CreateObject("System.Collections.ArrayList")
    Dim listDNP3Master As Object:   Set listDNP3Master = CreateObject("System.Collections.ArrayList")
    Dim listDNP3Slave As Object:    Set listDNP3Slave = CreateObject("System.Collections.ArrayList")
    Dim listAuxGateway As Object:   Set listAuxGateway = CreateObject("System.Collections.ArrayList")
    Dim listSisInfo As Object:      Set listSisInfo = CreateObject("System.Collections.ArrayList")

  
    For Each objWs In ThisWorkbook.Sheets
        ' Clear active filter.
        If objWs.FilterMode Then
            objWs.ShowAllData
        End If
        address = 0
        lastRow = objWs.Cells.SpecialCells(xlCellTypeLastCell).Row
        
        iColIncludedOnDatabase = GetColumnNumberByName(objWs, "INCLUIDO")
        iColType = GetColumnNumberByName(objWs, "TIPO")
        iColIED = GetColumnNumberByName(objWs, "IED", xlWhole)
        iColTag = GetColumnNumberByName(objWs, "TAG E3")
        iColDocstring = GetColumnNumberByName(objWs, "DESC")
        iColRef = GetColumnNumberByName(objWs, "REFE")

        For i = 1 To lastRow
            ' Verifica se ponto esta 'incluido na base de dados' de acordo com o preenchimento
            ' da coluna "INCLUIDO B.D.", e.g.: SIM|S|X
            Select Case UCase(Trim(objWs.Cells(i, iColIncludedOnDatabase).Value))
                Case "X", "S", "SIM"
                    If objWs.Cells(i, iColIncludedOnDatabase).Characters.Font.Strikethrough Then
                        isIncludedOnDatabase = False
                    Else
                        isIncludedOnDatabase = True
                    End If
                Case Else
                    isIncludedOnDatabase = False
            End Select
            
            sType = UCase(Trim(objWs.Cells(i, iColType).Value))
            sIED = UCase(Trim(objWs.Cells(i, iColIED).Value))
            sTag = UCase(Trim(objWs.Cells(i, iColTag).Value))
            sDocString = UCase(Trim(objWs.Cells(i, iColDocstring).Value))
            sRef = Trim(objWs.Cells(i, iColRef).Value)
            
            
            ' Confirma se ponto esta incluido na base e nenhum dos parametros mandatorios estao vazios.
            If (isIncludedOnDatabase) And (Not IsThereAnEmptyString(sType, sTag, sRef)) Then
                
                ' SisInfo
                If Not listSisInfo.Contains(sIED) Then
                    listSisInfo.Add sIED
                End If
                
                ' Tag
                Set objTag = New Tag
                
                Call objTag.Init(sType, sTag, sDocString, sRef)
                
                '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                ' IEC61850
                ' ObjectType;AdviseType;Name;AllowRead;AllowWrite;DocString;ParamDevice;ParamItem
                listIEC61850.Add Join(Array(objTag.ObjectType, _
                                            objTag.IEC61850.Item("AdviseType"), _
                                            objTag.Name, _
                                            objTag.IEC61850.Item("AllowRead"), _
                                            objTag.IEC61850.Item("AllowWrite"), _
                                            objTag.DocString, _
                                            objTag.IEC61850.Item("ParamDevice"), _
                                            objTag.IEC61850.Item("ParamItem") _
                                        ), ";")
                '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                ' DNP3Master
                ' ObjectType;AdviseType;Name;AllowRead;AllowWrite;DocString;EnableDeadband;N1;N2;N3;N4
                listDNP3Master.Add Join(Array( _
                                            objTag.ObjectType, _
                                            objTag.Master.Item("AdviseType"), _
                                            objTag.Name, _
                                            objTag.Master.Item("AllowRead"), _
                                            objTag.Master.Item("AllowWrite"), _
                                            objTag.DocString, _
                                            objTag.Master.Item("EnableDeadband"), _
                                            objTag.Master.Item("N1"), _
                                            objTag.Master.Item("N2"), _
                                            objTag.Master.Item("N3"), _
                                            address _
                                        ), ";")
                '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                ' DNP3Slave
                ' ObjectType;AdviseType;Name;AllowRead;AllowWrite;DocString;N1;N2;N3;N4;EnableDeadband;EnableDriverEvent;Source
                listDNP3Slave.Add Join(Array( _
                                            objTag.ObjectType, _
                                            objTag.Slave.Item("AdviseType"), _
                                            objTag.Name, _
                                            objTag.Slave.Item("AllowRead"), _
                                            objTag.Slave.Item("AllowWrite"), _
                                            objTag.DocString, _
                                            objTag.Slave.Item("N1"), _
                                            objTag.Slave.Item("N2"), _
                                            objTag.Slave.Item("N3"), _
                                            address, _
                                            objTag.Slave.Item("EnableDeadband"), _
                                            objTag.Slave.Item("EnableDriverEvent") _
                                        ), ";")
                
                If objTag.ObjectFunction <> "CD" Then
                    ' Associacao no slave exceto comandos
                    listDNP3Slave.Add "AgSimple;;" & objTag.Name & ".Links.Value;;;;;;;;;;Aquisicao.[61850].IEC61850." & objTag.Name & ".Value"
                Else
                    ' Lista de comandos p/ AuxGateway
                    ' ObjectType;Name;DocString;InputTag;OutputTag;CloseOutputValue;TripOutputValue
                    listAuxGateway.Add "xoComandoGenerico;" & sTag & ";" & objTag.DocString & _
                                        ";Distribuicao.DNP3Slave.Comandos." & sTag & _
                                        ";Aquisicao.[61850].IEC61850.Comandos." & sTag & _
                                        ";1;0"
                End If
                '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                address = address + 1
            End If
        Next
    Next
    
    ' IEC61850
    BuildListHeader listIEC61850, IEC61850
    MergeSisInfoIntoDriverList listSisInfo, listIEC61850, IEC61850
    ExportToCsv listIEC61850, ThisWorkbook.Path & "\IEC61850.csv"

    ' DNP3Master
    BuildListHeader listDNP3Master, DNP3Master
    MergeSisInfoIntoDriverList listSisInfo, listDNP3Master, DNP3Master
    ExportToCsv listDNP3Master, ThisWorkbook.Path & "\DNP3Master.csv"

    ' DNP3Slave
    BuildListHeader listDNP3Slave, DNP3Slave
    MergeSisInfoIntoDriverList listSisInfo, listDNP3Slave, DNP3Slave
    ExportToCsv listDNP3Slave, ThisWorkbook.Path & "\DNP3Slave.csv"
    
    ' AuxGateway
    BuildListHeader listAuxGateway, AuxGateway
    ExportToCsv listAuxGateway, ThisWorkbook.Path & "\AuxGateway.csv"
    
    CopyToClipboard ThisWorkbook.Path
    
    MsgBox "Fim", vbInformation
End Sub
Private Function GetColumnNumberByName(ByVal Sheet As Worksheet, ColumnName As String, Optional LookAt As XlLookAt = xlPart) As Integer
    Dim rng As Range
    Set rng = Sheet.Cells.EntireRow.Find(What:=ColumnName _
                                        , LookIn:=xlValues _
                                        , LookAt:=LookAt _
                                        , SearchOrder:=xlByColumns _
                                        , SearchDirection:=xlPrevious _
                                        , MatchCase:=False)
    
    If rng Is Nothing Then
        ' If rng isn't something (prevent errors).
        GetColumnNumberByName = 0
    Else
        ' Set to the first time a match is found.
        GetColumnNumberByName = rng.Column
    End If
End Function
Private Function IsThereAnEmptyString(ParamArray args() As Variant) As Boolean
    Dim arg As Variant
    Dim retval As Boolean: retval = False
    For Each arg In args
        If VarType(arg) <> vbString Then
            retval = True
            Exit For
        Else
            If Len(arg) = 0 Then
                retval = True
                Exit For
            End If
        End If
    Next
    IsThereAnEmptyString = retval
End Function
Private Sub ExportToCsv(ByVal ArrayList As Object, ByVal FilePath As String)
    Dim elem As Variant
    Dim binaryStream As Object
    
    With CreateObject("ADODB.Stream")
        .Type = 2: .Charset = "utf-8": .Open
        
        For Each elem In ArrayList
            .WriteText elem + vbCrLf
        Next

        Set binaryStream = CreateObject("ADODB.Stream")
        binaryStream.Type = 1: binaryStream.Mode = 3: binaryStream.Open
        .Position = 3: .CopyTo binaryStream        ' Skip BOM bytes
        .flush: .Close
        binaryStream.SaveToFile FilePath, 2
        binaryStream.Close
    End With
End Sub
Private Sub CopyToClipboard(ByVal Text As String)
    Dim obj As Object
    
    On Error Resume Next
    
    Set obj = CreateObject("new:{1C3B4210-F441-11CE-B9EA-00AA006B1A69}")
    obj.SetText Text
    obj.PutInClipboard
    Set obj = Nothing
    
    On Error GoTo 0
End Sub

Private Sub BuildListHeader(ByRef ArrayList As Object, ByVal IODriver As IODrivers)
    ' Adiciona cabecalho
    With ArrayList
        Select Case IODriver
            Case IODrivers.IEC61850
                .Insert 0, "ObjectType;AdviseType;Name;AllowRead;AllowWrite;DocString;ParamDevice;ParamItem"
            Case IODrivers.DNP3Master
                .Insert 0, "ObjectType;AdviseType;Name;AllowRead;AllowWrite;DocString;EnableDeadband;N1;N2;N3;N4"
            Case IODrivers.DNP3Slave
                .Insert 0, "ObjectType;AdviseType;Name;AllowRead;AllowWrite;DocString;N1;N2;N3;N4;EnableDeadband;EnableDriverEvent;Source"
            Case IODrivers.AuxGateway
                .Insert 0, "ObjectType;Name;DocString;InputTag;OutputTag;CloseOutputValue;TripOutputValue"
                Exit Sub
        End Select
        
        ' TODO:
        .Insert 1, "IOFolder;;Digitais"
        .Insert 2, "IOFolder;;Analogicos"
        .Insert 3, "IOFolder;;Comandos"
        .Insert 4, "IOFolder;;SisInfo"
    End With
End Sub

Private Sub MergeSisInfoIntoDriverList(ByVal SisInfoList As Object, ByRef DriverList As Object, ByVal IODriver As IODrivers)
    Dim elem As Variant
    Dim address As Integer
    
    ' offset
    address = 2500
    For Each elem In SisInfoList
        Select Case IODriver
            Case IODrivers.IEC61850
                ' ObjectType;AdviseType;Name;AllowRead;AllowWrite;DocString;ParamDevice;ParamItem
                DriverList.Add "IOTag;0;SisInfo." & elem & ";True;False;STATUS COMUNICACAO;" & elem & ";ServerStatus"
            Case IODrivers.DNP3Master
                ' ObjectType;AdviseType;Name;AllowRead;AllowWrite;DocString;EnableDeadband;N1;N2;N3;N4
                DriverList.Add "IOTag;1;SisInfo." & elem & ";True;False;STATUS COMUNICACAO;False;1;1;202;" & address
            Case IODrivers.DNP3Slave
                ' ObjectType;AdviseType;Name;AllowRead;AllowWrite;DocString;N1;N2;N3;N4;EnableDeadband;EnableDriverEvent;Source
                DriverList.Add "IOTag;0;SisInfo." & elem & ";False;True;STATUS COMUNICACAO;21;1;202;" & address & ";False;False"
                DriverList.Add "AgSimple;;SisInfo." & elem & ".Links.Value;;;;;;;;;;Abs(Aquisicao.[61850].IEC61850.SisInfo.[" & elem & "].Value <> 5)"
        End Select
        address = address + 1
    Next
End Sub

